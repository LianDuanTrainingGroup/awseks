# variables.tf
variable "eks_cluster_name" {
  description = "The name of the EKS cluster"
  type        = string
  default     = "eks-demo-cluster-01"
}

variable "region" {
  description = "The AWS region to deploy resources"
  type        = string
  default     = "us-east-1"
}

variable "availability_zone_a" {
  description = "The first availability zone to deploy resources"
  type        = string
  default     = "us-east-1a"
}

variable "availability_zone_b" {
  description = "The second availability zone to deploy resources"
  type        = string
  default     = "us-east-1b"
}

