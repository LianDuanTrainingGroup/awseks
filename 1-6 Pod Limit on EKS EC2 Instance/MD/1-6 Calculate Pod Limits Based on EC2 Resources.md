
<br>     
<br>
<br>   
<br>
<br>
<br> 
<br>     
<br>
<br>   
<br>
<br>
<br>      
🛰️

# **<image src='./images/eks.jpg' width="4%" hight="4%"/>Calculate AWS EKS Pod Limits Based on EC2 Node Resources**
- **<h3>⛳ EC2 t3.medium Instance Specifications:</h3>**
  - **🔲 CPU: 2 vCPUs**
  - **📏 Memory: 4 GB**
- **<h3>🚉 Max Deploy 11 Demo Nginx Pods:</h3>**
  - **🌿 Each Pod requests 0.125 CPU and 256 Mi Memory**
  - **🧮 Total Usage on EC2 Instance:**
     - **🔲 CPU: 1.965 vCPUs (1,965 millicore CPUs)**
     - **📏 Memory: 3,587MiB**
     - **🌐 IP Address: 16**


<br>     
<br>
<br>   
<br>
<br>
<br> 
<br>  
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>   
<br>
<br>
<br>      
🛰️

# **<image src='./images/eks.jpg' width="4%" hight="4%"/> Pod Limit on EKS EC2 Instance**
## **⭐ What We will Cover:**
- **<h3>🤖 EC2 Instance Types</h3>**
- **<h3>🔲 CPU Usage Calculation</h3>**
- **<h3>⛽ Memory Usage Calculation</h3>**
- **<h3>🌐 IP Address Usage Calculation</h3>**
- **<h3>👨‍🏫 Hands-on Demos</h3>**

<br>     
<br>
<br>   
<br>
<br>
<br> 
<br>     
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>
<br>
<br>      
🛰️

# **🤖 EC2 t3.medium Instance**  
- **📚 [Amazon EC2 Instance-Types](https://www.amazonaws.cn/en/ec2/instance-types/)**

- **📚 [IP addresses per network interface per instance type](https://github.com/awslabs/amazon-eks-ami/blob/master/files/eni-max-pods.txt)**

- **<h3>🖥️ EC2 t3.medium</h3>**
    - **🔲 2  CPU** 
    - **📏 4  GB Memory**
    - **🔑 17 IP Address**
<br>     
<br>
<br>   
<br>
<br>
<br> 
<br>     
<br>
<br>  
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>      
🛰️



# 🔲 CPU Usage Calculation
- **<h3>🐚 Pods: (12.5% * 1000m) * 11 = 1375m</h3>**
  - **📥 nginx-deployment.yaml=>limits=>cpu: "0.125"</h3>**
- **<h3>🅾️ Kubelet and OS: 2000Mi * 0.07% = 140m</h3>**
  - **🥝 Source: [Amazon EKS AMI Bootstrap Script](https://github.com/awslabs/amazon-eks-ami/blob/4b54ee95d42df8a2715add2a32f5150db097fde8/files/bootstrap.sh#L253-L262)**
  - **📽️ [AWS EKS Kubernetes Reserved Resources on EC2 Node]()** 
- **<h3>🐛 Daemonsets: 450m</h3>**
  - **🧑‍💻 kubectl describe node [node-name]** 
- **<h3>🧮 Total CPU Usage: 1965m</h3>**



<br>     
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>   
<br>
<br>
<br>
<br>     
<br>
<br>   
<br>
<br>
<br>      
🛰️


# ⛽ Memory Usage Calculation
- **<h3>Pods: 256Mi * 11 = 2816Mi</h3>**
  - **📥nginx-deployment.yaml=>limits=>memory: "256Mi"**
- **<h3>Kubelet: (11 * 16 + 255) = 431Mi</h3>**
  - **🥝 Source: [Amazon EKS AMI Bootstrap Script](https://github.com/awslabs/amazon-eks-ami/blob/4b54ee95d42df8a2715add2a32f5150db097fde8/files/bootstrap.sh#L253-L262)**
- **<h3>🐛 Daemonsets: 340Mi</h3>**
  - **🧑‍💻 kubectl describe node [node-name]**
- **<h3>🧮Total Memory Usage: 3587Mi</h3>**

<br>     
<br>
<br>   
<br>
<br>
<br> 
<br>  
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>   
<br>
<br>
<br>      
🛰️





# 🌐 IP Address Usage Calculation
- **<h3>🐚 Pods: 11 IP Addresses </h3>**
- **<h3>🐛 Daemonsets: 5 IP Addresses </h3>**
- **<h3>🧮 Total IP Address Usage: 16</h3>**


<br>     
<br>
<br>   
<br>
<br>
<br> 
<br>     
<br>
<br>  
<br>
<br>   
<br>
<br>
<br> 
<br>
<br>
<br>      
🛰️


# **🌇 Hands-on Demo Prerequisites**
  - **🚘 Running AWS EKS Cluster**
    - **📽️ [Create AWS ESK Using Terraform](https://youtu.be/ipPfi2TzPfc)**
    - **📽️ [Create EKS Cluster Using AWS Console](https://youtu.be/oDYtISiYFwk)**
- **🧪nginx-deployment.yaml**
- ```aws eks update-kubeconfig --region us-east-1 --name eks-demo-cluster-01```


<br>     
<br>
<br>   
<br>
<br>   
<br>
<br>
<br>
<br>
<br>
<br> 
<br>     
<br>
<br>   
<br>
<br>
<br>      
🛰️



# **🌇 Hands-on Demos**
## **How to Find EC2 Instance available IP address?**
- **🐊 Execution Steps:**
```
    kubectl get node
    kubectl describe node <node-name>
```
## **How to Find Daemonsets of CPU and Memory in EKS?**
- **🐊 Execution Steps:**
```
    kubectl get node
    kubectl describe node <node-name>
```
## **Test EC2 Node Capacity**
- **🐊 Execution Steps:**
``` 
    kubectl apply -f nginx-deployment.yaml  
    kubectl get pod
    kubectl describe pod <pod-name>          
```



