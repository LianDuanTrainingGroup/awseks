<br>
<br>   
<br>
<br>
<br>      
🎉

# **<image src='./images/eks.jpg' width="4%" hight="4%"/> AWS EKS Roles**
## **⭐ What We will Cover:**
- **🎭 AWS EKS Master Role**
- **👥 EC2 Work Group Role**
- **👨‍🏫 Hands-on Demos**

<br>
<br>   
<br>
<br>
<br>    
<br>
<br>   
<br>
<br>
<br>    
<br>
<br>   
<br>
<br>
<br>      
🎉


  

## **🎭 AWS EKS Master Role**
- **📦 Required for each cluster** 
- **💼 Manage nodes and the legacy Cloud Provider** 
- **🙆‍♂️ Create Elastic Load Balancing for services**
- **🛂 IAM Policy**
   - **AmazonEKSClusterPolicy**
- **📚 [Doc](https://docs.aws.amazon.com/eks/latest/userguide/service_IAM_role.html)**


<br>
<br>   
<br>
<br>
<br>    
<br>
<br>   
<br>
<br>
<br>    
<br>
<br>   
<br>
<br>
<br>      
🎉



## **👥 EC2 Work Group Role**
- **🎛 Launch nodes and register them into a cluster**
- **🎨 Node permissions for API call**
- **🛂 IAM Policies**
   - **AmazonEC2ContainerRegistryReadOnly**
   - **AmazonEKS_CNI_Policy**
   - **AmazonEKSWorkerNodePolicy**
- **📚 [Doc](https://docs.aws.amazon.com/eks/latest/userguide/create-node-role.html)**

<br>
<br>   
<br>
<br>
<br>    
<br>
<br>   
<br>
<br>
<br>    
<br>
<br>   
<br>
<br>
<br>      
🎉


## **👨‍🏫 Hands on Demos**
- **✍🏼Create an AWS EKS Master Role**
- **✍🏼Create an EC2 Work Group Role**

